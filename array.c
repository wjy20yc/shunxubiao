#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define TYPE int

// 设计数据结构
typedef struct Array
{
	TYPE* base; 	// 数组首地址
	size_t size;	// 元素的个数
}Array;

// 创建
Array* create_array(size_t len)
{
	Array* arr = malloc(sizeof(Array));
	arr->base = malloc(sizeof(TYPE)*len);
	arr->size = len;
	return arr;
}

// 销毁
void destory_array(Array* arr)
{
	free(arr->base);
	free(arr);
}

// 访问
TYPE* access_array(Array* arr,int index)
{
	if(index < 0 || index >= arr->size) return NULL;
	return arr->base + index;
}

// 查找
int find_array(Array* arr,TYPE data)
{
	for(int i=0; i<arr->size; i++)
	{
		if(data == arr->base[i])
			return i;
	}

	return -1;
}

// 排序
void sort_array(Array* arr)
{
	for(int i=0; i<arr->size-1; i++)
	{
		for(int j=i+1; j<arr->size; j++)
		{
			if(arr->base[i] > arr->base[j])
			{
				TYPE temp = arr->base[i];
				arr->base[i] = arr->base[j];
				arr->base[j] = temp;
			}
		}
	}
}

// 遍历
void show_array(Array* arr)
{
	 for(int i=0; i<arr->size; i++)
	 {
		printf("%d ",arr->base[i]);
	 }
	 printf("\n");
}

// 删除
bool delete_array(Array* arr,int index)
{
	if(index < 0 || index >= arr->size) return false;
	for(int i=index; i<arr->size-1; i++)
	{
		arr->base[i] = arr->base[i+1];
	}
	return true;
}

// 插入
bool insert_array(Array* arr,int index,TYPE data)
{
	if(index < 0 || index >= arr->size) return false;
	for(int i=arr->size-1; i>index; i--)
	{
		arr->base[i] = arr->base[i-1];
	}
	arr->base[index] = data;
	return true;
}

int main()
{
	Array* arr = create_array(20);
	for(int i=0; i<100; i++)
	{
		TYPE* p = access_array(arr,i);
		if(NULL != p) 
			*access_array(arr,i) = rand() % 100;
	}
	show_array(arr);//遍历显示顺序表
	sort_array(arr);//顺序表排序
	show_array(arr);
	printf("find index = %d\n",find_array(arr,86));//查找
	delete_array(arr,15);//删除
	delete_array(arr,15);
	printf("find index = %d\n",find_array(arr,86));
	show_array(arr);
	insert_array(arr,10,86);//指定位置下标插入元素
	printf("find index = %d\n",find_array(arr,86));
	show_array(arr);
	destory_array(arr);
}


